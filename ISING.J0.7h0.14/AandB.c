/*************************************************************************/
/*************  ISING MODEL OF MOLECULAR CRYSTAL NUCLEATION  *************/
/*************************************************************************/

#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>

#define N 32
#define T N*N*N*12
#define EQU N*N*N
#define J 0.70
#define h 0.14
#define redgeA 10.0
#define redgeB 100.0

#define DEBUG 1

/*************************************************************************/
/**********************  Function Declarations  **************************/
/*************************************************************************/

#include "./lattice2D-define.c"

/*************************************************************************/
/*****************************  MAIN  ************************************/
/*************************************************************************/

main(){

  int i, j, k, acc, count, seed;
  int x[4][N][N], xnew[4][N][N];
  int n[4], s[4], m[4], big[1+N*N][3];
  int hA0, hAT, hB0, hBT;
  float r[4];
  FILE *data, *shoots;

  system("date");
  seed = time(NULL);
  srand(seed);
  printf("\n N=%d h/kT=%.2f J/kT=%.2f", N, h, J);
  fflush(stdout);
  for(i=0;i<N;i=i+1){
    for(j=0;j<N;j=j+1){
      x[1][i][j] = -1;
      x[2][i][j] = -1;
    }
  }
  count = 0;
  while(count < EQU){
    count = count + 1;
    copy_state(x[2], x[1]);
    find_big_cluster(x[1], big);
    n[1] = big[0][0];
    s[1] = perimeter(x[1], big);
    printf("\n  shoot = (%d %d) r=%f", n[1], s[1], rxnc(n[1], s[1]));
    traj_segment(h, J, N*N, x[1], x[2]);
  }

  printf("\n");

}




